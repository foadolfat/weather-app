package folfat;


import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;

public class Weather
{
    private final String token = "1655f919bbcd29ed";
    private String zip;
    private JsonElement json;
    private JsonElement jsonCoords;


    public Weather(String zip)
    {
        if(zip.contains("coord"))
        {
            String latlon = zip.replace("coord", "");
            //this.zip = zip.replace("coord", "");
            this.zip = fetchCoords(latlon);
            System.out.println("phase 1");
            System.out.println(this.zip);
        }
        //Determine if zip code or State,City, or autoip
        else if  (!zip.matches("\\d+") && !zip.equals("autoip"))
        {
            //Change the results into an acceptable field
            String City = zip.substring(0,zip.indexOf(","));
            String State = zip.substring(zip.indexOf(",")+1);

            //Remove Leading/Trailing spaces
            State = State.trim();
            City = City.trim();

            //Enable to Cities to have spaces
            City = City.replace(" ","_");
            State = State.replace(" ","");

            //Code Testing
            System.out.println(City);
            System.out.println(State);

            this.zip =  State.toUpperCase()+ "/" + City;
        }
        else
        {
            this.zip = zip;
        }
    }

    public String getWeather()
    {
        return json.getAsJsonObject()
                .get("current_observation").getAsJsonObject()
                .get("weather").getAsString();
    }

    public String getFTemp()
    {
        return json.getAsJsonObject()
                .get("current_observation").getAsJsonObject()
                .get("temp_f").getAsString();
    }

    public String getCTemp()
    {
        return json.getAsJsonObject()
                .get("current_observation").getAsJsonObject()
                .get("temp_c").getAsString();
    }

    public String getState()
    {
        return json.getAsJsonObject()
                .get("current_observation").getAsJsonObject()
                .get("display_location").getAsJsonObject()
                .get("state").getAsString();
    }

    public String getCity()
    {
        return json.getAsJsonObject()
                .get("current_observation").getAsJsonObject()
                .get("display_location").getAsJsonObject()
                .get("city").getAsString();
    }

    public String getZip()
    {
        return json.getAsJsonObject()
                .get("current_observation").getAsJsonObject()
                .get("display_location").getAsJsonObject()
                .get("zip").getAsString();
    }

    public String getWeatherIcon()
    {
        return json.getAsJsonObject()
                .get("current_observation").getAsJsonObject()
                .get("icon_url").getAsString();
    }

    public String getMap(String satToken, int radius, boolean isRadar)
    {
        if(!isRadar)
        return "http://api.wunderground.com/api/1655f919bbcd29ed/satellite/q/" + satToken + ".png?radius="+ radius +"&width=709&height=297";
        else
            return "http://api.wunderground.com/api/1655f919bbcd29ed/radar/q/" + satToken + ".png?newmaps=1&radius="+ radius +"&width=709&height=297";
    }



    public String getForecastDate(int index)
    {
        String date = json.getAsJsonObject()
                .get("forecast").getAsJsonObject()
                .get("simpleforecast").getAsJsonObject()
                .get("forecastday").getAsJsonArray()
                .get(index).getAsJsonObject()
                .get("date").getAsJsonObject()
                .get("pretty").getAsString();
        return date.substring(date.indexOf("on ") + 3);

    }

    public String getForecastWeekday(int index)
    {
        return  json.getAsJsonObject()
                .get("forecast").getAsJsonObject()
                .get("simpleforecast").getAsJsonObject()
                .get("forecastday").getAsJsonArray()
                .get(index).getAsJsonObject()
                .get("date").getAsJsonObject()
                .get("weekday").getAsString();
    }

    public String getForeSat(int index)
    {
        return  json.getAsJsonObject()
                .get("forecast").getAsJsonObject()
                .get("simpleforecast").getAsJsonObject()
                .get("forecastday").getAsJsonArray()
                .get(index).getAsJsonObject()
                .get("icon_url").getAsString();
    }

    public String getForeCastHighF(int index)
    {
        return json.getAsJsonObject()
                .get("forecast").getAsJsonObject()
                .get("simpleforecast").getAsJsonObject()
                .get("forecastday").getAsJsonArray()
                .get(index).getAsJsonObject()
                .get("high").getAsJsonObject()
                .get("fahrenheit").getAsString();
    }

    public String getForeCastLowF(int index)
    {
        return json.getAsJsonObject()
                .get("forecast").getAsJsonObject()
                .get("simpleforecast").getAsJsonObject()
                .get("forecastday").getAsJsonArray()
                .get(index).getAsJsonObject()
                .get("low").getAsJsonObject()
                .get("fahrenheit").getAsString();
    }

    public String getForeCastLowC(int index)
    {
        return json.getAsJsonObject()
                .get("forecast").getAsJsonObject()
                .get("simpleforecast").getAsJsonObject()
                .get("forecastday").getAsJsonArray()
                .get(index).getAsJsonObject()
                .get("low").getAsJsonObject()
                .get("celsius").getAsString();
    }

    public String getForeCastHighC(int index)
    {
        return json.getAsJsonObject()
                .get("forecast").getAsJsonObject()
                .get("simpleforecast").getAsJsonObject()
                .get("forecastday").getAsJsonArray()
                .get(index).getAsJsonObject()
                .get("high").getAsJsonObject()
                .get("celsius").getAsString();
    }

    public double[] getCoords()
    {
        double lat = json.getAsJsonObject()
            .get("current_observation").getAsJsonObject()
            .get("display_location").getAsJsonObject()
            .get("latitude").getAsDouble();
        double lon = json.getAsJsonObject()
                .get("current_observation").getAsJsonObject()
                .get("display_location").getAsJsonObject()
                .get("longitude").getAsDouble();

        return (new double[]{lat, lon});
    }



    /*
    public Double getLatX(Double distance)
    {
        double oldLat = json.getAsJsonObject()
                .get("current_observation").getAsJsonObject()
                .get("display_location").getAsJsonObject()
                .get("longitude").getAsDouble();
        double newLat = oldLat + (distance/111111);
        return newLat;
    }

    public Double getLongY(Double distance)
    {
        double oldLong = json.getAsJsonObject()
                .get("current_observation").getAsJsonObject()
                .get("display_location").getAsJsonObject()
                .get("latitude").getAsDouble();
        double newLong = oldLong + distance/(111111*(Math.cos(oldLong)));
        return newLong;
    }
    */

    public String fetchCoords(String latlon)
    {
        String wuRequestCoords = "http://api.wunderground.com/api/" + token + "/geolookup/q/" + latlon + ".json";
        try
        {
            URL wuUrl = new URL(wuRequestCoords);

            InputStream is = wuUrl.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            JsonParser parser = new JsonParser();
            jsonCoords = parser.parse(br);
        }
        catch (java.net.MalformedURLException mue)
        {
            System.out.println("URL not valid");
            System.exit(1);
        }
        catch (java.io.IOException ioe)
        {
            System.out.println("IO Exception Caught");
            System.exit(1);
        }
        System.out.println(wuRequestCoords);
        System.out.print("phase 2");
        return jsonCoords.getAsJsonObject()
                //.get("features").getAsJsonObject()
                //.get("geolookup").getAsJsonObject()
                .get("location").getAsJsonObject()
                .get("zip").getAsString();
    }



    public void fetch()
    {
        String wuRequest = "http://api.wunderground.com/api/" + token + "/conditions/forecast7day/q/" + zip + ".json";
        try
        {
            URL wuUrl = new URL(wuRequest);

            InputStream is = wuUrl.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            JsonParser parser = new JsonParser();
            json = parser.parse(br);
        }
        catch (java.net.MalformedURLException mue)
        {
            System.out.println("URL not valid");
            System.exit(1);
        }
        catch (java.io.IOException ioe)
        {
            System.out.println("IO Exception Caught");
            System.exit(1);
        }

    }
}