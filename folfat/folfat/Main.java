package folfat;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


public class Main extends Application{


    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Weather App");
        primaryStage.getIcons().add(new Image("https://lh3.googleusercontent.com/9sTzeo_kaN1bFVNwbzqAB3BhbJVLvdplvN18s4j_GWs8wTVA2A-ILyc3lr3spHDcEKs"));
        primaryStage.setScene(new Scene(root, 1090, 537));
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
