package folfat;

import javafx.scene.image.Image;

public class Forecast {

    private String forecastDay;
    private String forecastDate;
    private String forecastHighF;
    private String forecastLowF;
    private String forecastHighC;
    private String forecastLowC;
    private Image forecastIcon;

    public Forecast(Weather w, int index)
    {
        forecastDate = w.getForecastDate(index);
        forecastHighF = w.getForeCastHighF(index);
        forecastLowF = w.getForeCastLowF(index);
        forecastHighC = w.getForeCastHighC(index);
        forecastLowC = w.getForeCastLowC(index);
        forecastDay = w.getForecastWeekday(index);
        forecastIcon = new Image(w.getForeSat(index));
    }

    public String getForecastTextF()
    {
        return forecastDay + "\n\n" + forecastDate + "\n" + "Highs of " + forecastHighF + "˚F" + "\n" + "Lows of " + forecastLowF + "˚F";
    }

    public String getForecastTextC()
    {
        return forecastDay + "\n\n" + forecastDate + "\n" + "Highs of " + forecastHighC + "˚C" + "\n" + "Lows of " + forecastLowC + "˚C";
    }

    public Image getforecastImage()
    {
        return forecastIcon;
    }


}
