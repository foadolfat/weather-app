package folfat;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import com.google.gson.JsonElement;
import org.controlsfx.control.textfield.TextFields;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class GUI {
    private JsonElement autoComplete;
    @FXML
    TextField zipField;
    @FXML
    Text currentWeatherText;
    @FXML
    Button goButton;
    @FXML
    ImageView weatherIcon;
    @FXML
    Button clearButton;
    @FXML
    Button tempButton;
    @FXML
    Button satOrRadar;
    @FXML
    ImageView satImage;
    @FXML
    Text forecast1;
    @FXML
    Text forecast2;
    @FXML
    Text forecast3;
    @FXML
    Text forecast4;
    @FXML
    Text forecast5;
    @FXML
    Text forecast6;
    @FXML
    Text forecast7;
    @FXML
    ImageView forecastIconView1;
    @FXML
    ImageView forecastIconView2;
    @FXML
    ImageView forecastIconView3;
    @FXML
    ImageView forecastIconView4;
    @FXML
    ImageView forecastIconView5;
    @FXML
    ImageView forecastIconView6;
    @FXML
    ImageView forecastIconView7;
    @FXML
    Line topLine;
    @FXML
    Line botLine;
    @FXML
    Line leftLine;
    @FXML
    Line rightLine;



    private String f;
    private String c;
    private Weather w;
    private boolean isF;
    private Image sat;
    private String zip;
    private boolean isRadar;
    private int radius;


    public void initialize() {
        zipField.setText("autoip");
        handleGoButton();
        handleGoButton();

    }
    public void handleAutoComplete()
    {
        TextFields.bindAutoCompletion(zipField,getAutoComplete(zipField.getText()));
    }
    //go
    public void handleGoButton() {
        zip = zipField.getText();
        goButton.requestFocus();
        radius = 300;
        try {
            w = new Weather(zip);
            w.fetch();
            zipField.setText(w.getZip());
            f = w.getFTemp();
            c = w.getCTemp();
            currentWeatherText.setText(w.getCity() + ", " + w.getState() + "\n" + w.getWeather() + "\n\n" + f + "˚F");
            isF = true;
            Text[] forecastTexts = {forecast1, forecast2, forecast3, forecast4, forecast5, forecast6, forecast7};
            ImageView[] forecastIcons = {forecastIconView1, forecastIconView2, forecastIconView3, forecastIconView4, forecastIconView5, forecastIconView6, forecastIconView7};
            for (int i = 0; i < 7; i++) {
                Forecast f = new Forecast(w, i);
                forecastTexts[i].setText(f.getForecastTextF());
                forecastIcons[i].setImage(f.getforecastImage());
            }
            Image image = new Image(w.getWeatherIcon());
            weatherIcon.setImage(image);
            satImage.setImage(sat);
            tempButton.setVisible(true);
            tempButton.setText("Celsius");
            satOrRadar.setText("Radar");
            isRadar = false;
            satOrRadar.setVisible(true);
            errorBox(false);
            paintMap(isRadar);
        } catch (NullPointerException npe) {
            handleClearButton();
            zipField.setPromptText("Invalid ZIP");
            errorBox(true);
            tempButton.setVisible(false);

        } catch (StringIndexOutOfBoundsException siobe) {
            handleClearButton();
            zipField.setPromptText("Invalid ZIP");
            errorBox(true);
            tempButton.setVisible(false);
        }

    }

    /*
    private class background extends AsyncTask<String, Weather>
    {
        public Weather doInBackground(String url)
        {
            Weather w = new Weather(zip);
            w.fetch();
            return w;
        }
        public void onPostExecute(Weather w)
        {
            weatherField.setText("" + w.getWeather());
        }
    }
    */

    public void handleKeyReleased(KeyEvent event) {
        event.consume();
        if (event.getCode().equals(KeyCode.ENTER)) {
            zipField.setText(zipField.getText());
            handleGoButton();
        } else {
            zipField.setText(zipField.getText() + event.getCharacter());
            zipField.positionCaret(zipField.getLength());
        }
    }
    //AutoComplete
    public String[] getAutoComplete(String query) {
        String acRequest = "http://autocomplete.wunderground.com/aq?query=" + query;
        try {
            URL acURL = new URL(acRequest);
            InputStream is = acURL.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            JsonParser parser = new JsonParser();
            autoComplete = parser.parse(br);
        } catch (java.net.MalformedURLException mue) {
            System.out.println("URL not valid");
        } catch (java.io.IOException ioe) {
            System.out.println("IO Exception Caught");
        }
        //Search Through Array
        JsonArray acArray = autoComplete.getAsJsonObject()
                .get("RESULTS").getAsJsonArray();
        String[] returnArray = new String[acArray.size()];
        for (int i = 0; i < acArray.size(); i++)
        {
            returnArray[i] = autoComplete.getAsJsonObject()
                    .get("RESULTS").getAsJsonArray()
                    .get(i).getAsJsonObject()
                    .get("name").getAsString();
        }
        return returnArray;
    }

    public void handleClearButton() {
        try {
            satOrRadar.setVisible(false);
            zipField.setText("");
            currentWeatherText.setText("");
            weatherIcon.setImage(null);
            tempButton.setVisible(false);
            satImage.setImage(null);
            errorBox(false);
            for (int i = 0; i < 7; i++) {
                Text[] forecastTexts = {forecast1, forecast2, forecast3, forecast4, forecast5, forecast6, forecast7};
                ImageView[] forecastIcons = {forecastIconView1, forecastIconView2, forecastIconView3, forecastIconView4, forecastIconView5, forecastIconView6, forecastIconView7};
                forecastTexts[i].setText("");
                forecastIcons[i].setImage(null);
            }


        } catch (NullPointerException npe) {
            System.out.println("For some reason this string needs to be here.");
        }
        //catch(NullPointerException npe) {System.out.println("For some reason this string needs to be here."); }
    }

    public void handleTempButton(ActionEvent e) {
        Text[] forecastTexts = {forecast1, forecast2, forecast3, forecast4, forecast5, forecast6, forecast7};
        ImageView[] forecastIcons = {forecastIconView1, forecastIconView2, forecastIconView3, forecastIconView4, forecastIconView5, forecastIconView6, forecastIconView7};

        if (isF) {
            tempButton.setText("Fahrenheit");
            currentWeatherText.setText(w.getCity() + ", " + w.getState() + "\n" + w.getWeather() + "\n\n" + c + "˚C");
            for (int i = 0; i < 7; i++) {
                Forecast f = new Forecast(w, i);
                forecastTexts[i].setText(f.getForecastTextC());
                forecastIcons[i].setImage(f.getforecastImage());
            }
            isF = false;

        } else {
            tempButton.setText("Celsius");
            currentWeatherText.setText(w.getCity() + ", " + w.getState() + "\n" + w.getWeather() + "\n\n" + f + "˚F");
            for (int i = 0; i < 7; i++) {
                Forecast f = new Forecast(w, i);
                forecastTexts[i].setText(f.getForecastTextF());
                forecastIcons[i].setImage(f.getforecastImage());
            }
            isF = true;

        }
    }

    public void handleSatOrRadarButton() {
        isRadar = !isRadar;
        if (!isRadar) satOrRadar.setText("Radar");
        else satOrRadar.setText("Satellite");
        paintMap(isRadar);
    }

    public void handleScroll(ScrollEvent event) {
        if (radius - event.getDeltaY() > 750 || radius - event.getDeltaY() < 50) paintMap(isRadar);
        else {
            radius = radius - (int) event.getDeltaY();
            paintMap(isRadar);
        }
    }

    public void paintMap(boolean b) {
        sat = new Image(w.getMap(zip, radius, b));
        satImage.setImage(sat);
    }

    public void errorBox(boolean v) {
        topLine.setVisible(v);
        botLine.setVisible(v);
        leftLine.setVisible(v);
        rightLine.setVisible(v);
    }

    public void handleClick(MouseEvent m) {

        System.out.println("Needs further work");

        /*
        double deltaX = 150 - m.getX();
        double deltaY = 150 - m.getY();
        double distance = 146; //Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
        double slope = deltaY / deltaX;
        double bearing =-139; //ath.pow(Math.tan(slope), -1); //Bearing calculation = WRONG
        double oldLat = degToRad(38.790648);
        double oldLon = degToRad(-121.377452);
        double[] newCoords = pointRadialDistance(oldLat, oldLon, bearing, distance);
        zipField.setText("coord" + radToDeg(newCoords[0]) + "," + radToDeg(newCoords[1]));
        //handleGoButton();
        //zipField.setText(w.fetchCoords(radToDeg(newCoords[0]), radToDeg(newCoords[1])));

        System.out.println("deltaX= " + deltaX + "\ndeltaY= " + deltaY);
        System.out.println("Distance= " + distance);
        System.out.println("Bearing= " + bearing);
        System.out.println("Old latitude= " + radToDeg(oldLat) + "\nOld longitude= " + radToDeg(oldLon));
        System.out.println("New latitude= " + radToDeg(newCoords[0]) + "\nNew longitude= " + radToDeg(newCoords[1]) + "\n\n");
        //System.out.println(w.fetchCoords(radToDeg(newCoords[0]), radToDeg(newCoords[1])));
        */


    }

    public double degToRad(double angle) {
        return angle * (Math.PI / 180);
    }

    public double radToDeg(double radian) {
        return radian * (180 / Math.PI);
    }

    private static double[] pointRadialDistance(double lat1, double lon1,
                                                double radianBearing, double radialDistance) {
        double lat = Math.asin(Math.sin(lat1) * Math.cos(radialDistance) + Math.cos(lat1)
                * Math.sin(radialDistance) * Math.cos(radianBearing));
        double lon;
        if (Math.cos(lat) == 0) {  // Endpoint a pole
            lon = lon1;
        } else {
            lon = ((lon1 - Math.asin(Math.sin(radianBearing) * Math.sin(radialDistance) / Math.cos(lat)) //long calculations = WRONG
                    + Math.PI) % (2 * Math.PI)) - Math.PI;
        }
        return (new double[]{lat, lon});
    }
/*
    private class GetDataInBackground extends AsyncTask<String, Weather> {
        @Override
        protected Weather doInBackground(String zip) {// Fetch the weather data
            Weather w = new Weather(zip);
            w.fetch();
            return w;
        }

        @Override
        protected void onPostExecute(Weather w)
        {
            // Update the data on the screen
            w.fetch();
        }
    }
    */
}